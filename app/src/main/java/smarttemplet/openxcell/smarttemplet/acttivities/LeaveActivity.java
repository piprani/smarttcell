package smarttemplet.openxcell.smarttemplet.acttivities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import smarttemplet.openxcell.smarttemplet.R;
import smarttemplet.openxcell.smarttemplet.databinding.ActivityLeaveBinding;

public class LeaveActivity extends BaseActivity {


    ActivityLeaveBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_leave);
        setToolbarNew(mBinding.llToolbar.toolbar, getString(R.string.app_name), false);
        mBinding.tvLeaveType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LeaveActivity.this, LeaveTypeActivity.class));
            }
        });
    }
}
