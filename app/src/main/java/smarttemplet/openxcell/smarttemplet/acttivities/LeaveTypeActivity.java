package smarttemplet.openxcell.smarttemplet.acttivities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import smarttemplet.openxcell.smarttemplet.R;
import smarttemplet.openxcell.smarttemplet.adapters.LeaveTypeAdapter;
import smarttemplet.openxcell.smarttemplet.databinding.ActivityLeavetypeBinding;

public class LeaveTypeActivity extends BaseActivity {

    ActivityLeavetypeBinding mBinding;
    LeaveTypeAdapter adapter;
    List<String> leaveTypes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_leavetype);
        setToolbarNew(mBinding.llToolbar.toolbar, getString(R.string.title_leave_type), false);

        addDummyLeaveType();
        adapter = new LeaveTypeAdapter(LeaveTypeActivity.this, leaveTypes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(LeaveTypeActivity.this);
        mBinding.recycleView.setLayoutManager(layoutManager);
        mBinding.recycleView.setHasFixedSize(true);// Letting the system know that the list objects are of fixed size
        mBinding.recycleView.setAdapter(adapter);
    }

    private void addDummyLeaveType() {
        leaveTypes.clear();
        leaveTypes.add("Planned Leave");
        leaveTypes.add("Unplanned Leave");
        leaveTypes.add("Leave Cancellation");
        leaveTypes.add("Half Day Leave");
        leaveTypes.add("Early Going");
        leaveTypes.add("Compensation of Early Going");
        leaveTypes.add("In Punch Missing");
        leaveTypes.add("Out Punch Missing");
        leaveTypes.add("Comp-Off");
        leaveTypes.add("Own Marriage Leave");
        leaveTypes.add("Paternity Leave");
        leaveTypes.add("Bereavement Leave");
        leaveTypes.add("Maternity Leave");


    }
}
