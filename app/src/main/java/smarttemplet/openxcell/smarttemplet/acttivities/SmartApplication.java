package smarttemplet.openxcell.smarttemplet.acttivities;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

import smarttemplet.openxcell.smarttemplet.utility.ImagePipelineConfigFactory;


public class SmartApplication extends Application {

    ImagePipelineConfig imagePipelineConfig;

    @Override
    public void onCreate() {
        super.onCreate();
        initFresco();
    }


    /*
        inital Fresco Image loading library
     */
    private void initFresco() {
        imagePipelineConfig = ImagePipelineConfigFactory.getImagePipelineConfig(this);
        Fresco.initialize(this, imagePipelineConfig);
    }

}
