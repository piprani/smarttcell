package smarttemplet.openxcell.smarttemplet.acttivities;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;

import smarttemplet.openxcell.smarttemplet.R;
import smarttemplet.openxcell.smarttemplet.databinding.ActivityMainBinding;

public class MainActivty extends BaseActivity {


    // dm pic url :https://workhive.s3.amazonaws.com/user/workhivedp_1414485582.jpg
    // pm pic url :http://workhive.s3.amazonaws.com/2/user/1780_1528100026.jpg
    String pmUrl = "http://workhive.s3.amazonaws.com/2/user/1780_1528100026.jpg";
    String dmUrl = "https://workhive.s3.amazonaws.com/user/workhivedp_1414485582.jpg";
    String userUrl = "http://workhive.s3.amazonaws.com/2/user/javid_383.jpg";

    ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setToolbarNew(mBinding.llToolbar.toolbar, getString(R.string.app_name), false);
        setValues();
        setUserProfile(userUrl);
    }

    private void setValues() {
        mBinding.cardPm.ivCardUser.setImageURI(Uri.parse(pmUrl));
        mBinding.cardDm.ivCardUser.setImageURI(Uri.parse(dmUrl));


    }
}
