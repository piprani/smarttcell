package smarttemplet.openxcell.smarttemplet.acttivities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import smarttemplet.openxcell.smarttemplet.R;

public class BaseActivity extends AppCompatActivity {
    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setToolbarNew(Toolbar toolbar, String title, boolean isBackEnabled) {
       /* setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (isBackEnabled) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();

                }
            });
        }


        TextView tvTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (tvTitle != null)
            tvTitle.setText(title);*/


        this.toolbar = toolbar;
        setSupportActionBar(this.toolbar);

        getSupportActionBar().setHomeAsUpIndicator(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (isBackEnabled) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
//            this.toolbar.setNavigationIcon(R.drawable.ic_back);
            this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        TextView tvTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (tvTitle != null)
            tvTitle.setText(title);

    }


    public void setUserProfile(String url) {

        if (toolbar != null) {
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) toolbar.findViewById(R.id.ivUserPic);
            if (simpleDraweeView != null) {
                simpleDraweeView.setVisibility(View.VISIBLE);
                simpleDraweeView.setImageURI(Uri.parse(url));
            }
        }

    }
}
