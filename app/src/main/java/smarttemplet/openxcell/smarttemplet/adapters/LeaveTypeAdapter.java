package smarttemplet.openxcell.smarttemplet.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import smarttemplet.openxcell.smarttemplet.R;
import smarttemplet.openxcell.smarttemplet.databinding.RawLeaveTypeBinding;


/**
 * Created by root on 10/5/17.
 */

public class LeaveTypeAdapter extends RecyclerView.Adapter<LeaveTypeAdapter.BindingHolder> {


    public List<String> mList = new ArrayList<>();
    Context mContext;

    public LeaveTypeAdapter() {
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }


    public LeaveTypeAdapter(Context context, List<String> list) {
        mContext = context;
        this.mList = list;

    }

    @Override
    public void onBindViewHolder(final BindingHolder holder, final int position) {

        RawLeaveTypeBinding mbinding = (RawLeaveTypeBinding) holder.getBinding();

        mbinding.tvLeaveType.setText(mList.get(position));

    }


    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BindingHolder holder;
        View v = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        v = inflater.inflate(R.layout.raw_leave_type, parent, false);
        holder = new BindingHolder(v);

        return holder;
    }


    public void setList(List<String> list) {
        this.mList = list;
        notifyDataSetChanged();
    }


}
